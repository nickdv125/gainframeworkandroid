## Introduction

The Gain Fitness Android SDK is an Android Archive (aar) written in Kotlin.  It allows your application to provide the GAIN Fitness workout mode experience to your apps users.  An API Key is required.  Please contact support-sdk@gainfitness.com to obtain an API Key.

## Requirements

The Gain Fitness Android SDK supports API 16 and up. Data binding is required in your project. The SDK also uses androidx libraries.

## Installation / Setup

1. Enable databinding in your `app/build.gradle`

```
android {
    ..
    buildFeatures {
        dataBinding true
    }
    ..
}
```

2. Enable libs directory for storing aar in your project's `build.gradle`. Put 'gain-fitness-sdk-1.0.1.aar' in `app/libs`. This location be changed if needed.

```
    allprojects {
        repositories {
            jcenter()
            flatDir {
                dirs 'libs'
            }
        }
    }
```

3. Make sure your project is compatible with Java 1.8. Add the following in your app's `build.gradle`.

```
compileOptions {
    sourceCompatibility JavaVersion.VERSION_1_8
    targetCompatibility JavaVersion.VERSION_1_8
}
```

4. Include the GAIN Fitness Android SDK aar in your `app/build.gradle`.
`implementation(name:'gain-fitness-sdk-1.0.1', ext:'aar')`

5. Include the dependencies for GAIN Fitness Android SDK in your `app/build.gradle`.

```
implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0"
implementation 'androidx.appcompat:appcompat:1.2.0'
implementation "androidx.navigation:navigation-fragment-ktx:2.3.0"
implementation "androidx.navigation:navigation-ui-ktx:2.3.0"
implementation "com.squareup.moshi:moshi-kotlin:1.9.3"
implementation "com.squareup.okhttp3:okhttp:4.8.1"
implementation "io.coil-kt:coil-base:0.12.0"
implementation 'androidx.swiperefreshlayout:swiperefreshlayout:1.1.0'
implementation 'androidx.constraintlayout:constraintlayout:2.0.2'
```

6. Add the necessary theme color to your `colors.xml`.

`<color name="gain_theme_color">#429CF9</color>`

## Usage

1. Setup GAIN Fitness SDK.

```
GainFitnessSDK.initialize("YOUR_GAIN_API_KEY", R.color.gain_theme_color);
GainFitnessSDK.setEventListener(this);
GainFitnessSDK.setErrorListener(this);
```

2. Trigger the GAIN Fitness flow in your app at an appropriate time.

`GainFitnessSDK.start(context);`

3. Listen for events and errors as needed.

```
@Override
public void loggedEvent(@NotNull GainFitnessSDKEvent gainFitnessSDKEvent, @Nullable Map<String, String> map) {
    // Events
}

@Override
public void loggedError(@NotNull GainFitnessSDKError gainFitnessSDKError) {
    // Errors
}
```

## Events

The Gain SDK sends the following events.

```
// Gain module was activated (i.e. Workout list was shown) - No Parameters
GainFitnessSDKEvent.INITIALIZED

// A workout was selected from the Workoust list - Parameters: "workoutName"
GainFitnessSDKEvent.VIEWED_WORKOUT_PREVIEW

// A workout was started - Parameters: "workoutName"
GainFitnessSDKEvent.STARTED_WORKOUT 

// A workout was completed - Parameters: "workoutName", appState (`true` if active or `false` if in background)
GainFitnessSDKEvent.COMPLETED_WORKOUT
```